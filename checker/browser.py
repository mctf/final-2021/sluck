import sys
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from api import STATUS_UP, STATUS_CORRUPT


def pull_flag(ip: str, username: str, password: str, channel_id: str, flag: str):
    try:
        chrome_options = Options()
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options)

        start_url = "http://" + ip
        driver.get(start_url)

        login_input = driver.find_element(By.ID, 'login-form-login-input')
        login_input.send_keys(username)

        password_input = driver.find_element(By.ID, 'login-form-password-input')
        password_input.send_keys(password)

        driver.find_element(By.ID, 'login-button').click()

        driver.implicitly_wait(5)

        driver.find_element(By.ID, f'open-channel-{channel_id}-button').click()


        try:
            WebDriverWait(driver, 1500).until(
                EC.text_to_be_present_in_element((By.ID, "chat-messages-box"), flag))
            found = True
        except TimeoutException:
            print("Timed out waiting for flag", file=sys.stderr)
            found = False

        if found:
            print(STATUS_UP)
            print('Everything is fine')
        else:
            messages = driver.find_element(By.ID, 'chat-messages-box').text
            print(STATUS_CORRUPT)
            print('Flag not found')
            print(messages, file=sys.stderr)

        driver.quit()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during flag pull, could be changed html selectors')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)
