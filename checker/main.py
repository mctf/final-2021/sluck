from api import check_functionality, push_flag
from browser import pull_flag

ip = 'service0.team0.services.mctf.online'

check_functionality(ip)
username, password, channel_id = push_flag(ip, 'mctf{test_flag}')
pull_flag(ip, username, password, channel_id, 'mctf{test_flag}')
