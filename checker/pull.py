from browser import pull_flag
import sys
from redis import StrictRedis

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
hostname = sys.argv[3]
oldFlag = sys.argv[4]

redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)

username = redis.get(f'checkers_state/sluck/{hostname}/username')
password = redis.get(f'checkers_state/sluck/{hostname}/password')
channel_id = redis.get(f'checkers_state/sluck/{hostname}/channel_id')
pull_flag(hostname, username, password, channel_id, oldFlag)
