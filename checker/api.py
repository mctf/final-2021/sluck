import base64
import string
import sys
from random import randint, choice

import requests
from user_agent import generate_user_agent

STATUS_UP = 'UP'
STATUS_DOWN = 'DOWN'
STATUS_CORRUPT = 'CORRUPT'
STATUS_MUMBLE = 'MUMBLE'

timeout = 5

ua = generate_user_agent()


class CheckException(Exception):
    def __init__(self, status, description, error):
        self.status = status
        self.description = description
        self.error = error


def random_string(length: int):
    chars = string.ascii_letters + string.digits
    return ''.join(choice(chars) for _ in range(length))


def register(session: requests.Session, url: str):
    username = random_string(randint(4, 20))
    password = random_string(randint(10, 40))
    encoded_password = base64.b64encode(password.encode('ascii')).decode("utf-8")

    try:
        r = session.post(url + 'api/user/register',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "username": username,
                             "encodedPassword": encoded_password
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/user/register returned code {r.status_code}', r.text)

        token = r.json()['token']

        return username, password, token
    except Exception as e:
        raise CheckException(STATUS_DOWN, 'Can\'t send register request', str(e))


def do_login(session: requests.Session, url: str, username: str, password: str) -> str:
    try:
        r = session.post(url + 'api/user/login',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "username": username,
                             "encodedPassword": base64.b64encode(password.encode('ascii')).decode("utf-8")
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/user/login returned code {r.status_code}', r.text)

        token = r.json()['token']

        return token
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send login request', str(e))


def create_channel(session: requests.Session, url: str):
    name = random_string(randint(6, 22))

    try:
        r = session.post(url + 'api/channel',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "name": name
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'POST /channel returned code {r.status_code}', r.text)

        return name
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send create channel request', str(e))


def get_channel_list(session: requests.Session, url: str):
    try:
        r = session.get(url + 'api/channel',
                        timeout=timeout,
                        headers={
                            "User-Agent": ua
                        })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'GET /channel returned code {r.status_code}', r.text)

        return r.json()
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send get channel list request', str(e))


def set_welcome_message(session: requests.Session, url: str, channel_id: str, message: str):
    try:
        r = session.post(url + 'api/channel/welcomeMessage',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "channelId": channel_id,
                             "welcomeMessage": message
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'POST /channel/welcomeMessage returned code {r.status_code}', r.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send set welcome message request', str(e))


def invite_user(session: requests.Session, url: str, channel_id: str, username: str):
    try:
        r = session.post(url + 'api/channel/invite',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "channelId": channel_id,
                             "username": username
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'POST /channel/invite returned code {r.status_code}', r.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send invite request', str(e))


def send_message(session: requests.Session, url: str, channel_id: str, message: str):
    try:
        r = session.post(url + 'api/channel/' + channel_id,
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         },
                         json={
                             "text": message,
                         })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'POST /channel/{{id}} returned code {r.status_code}', r.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send new message request', str(e))


def get_messages(session: requests.Session, url: str, channel_id: str) -> list:
    try:
        r = session.get(url + 'api/channel/' + channel_id,
                        timeout=timeout,
                        headers={
                            "User-Agent": ua
                        })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'GET /channel/{{id}} returned code {r.status_code}', r.text)

        return r.json()
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send get channel messages list request', str(e))


def get_my_profile(session: requests.Session, url: str) -> dict:
    try:
        r = session.get(url + 'api/user/me',
                        timeout=timeout,
                        headers={
                            "User-Agent": ua
                        })

        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'GET /channel/{{id}} returned code {r.status_code}', r.text)

        return r.json()
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send get channel messages list request', str(e))


def check_functionality(ip: str):
    try:
        url = 'http://' + ip + '/'

        s1 = requests.Session()
        username1, password1, token1 = register(s1, url)
        s1.headers = {'Authorization': 'Bearer {}'.format(token1)}

        channel_name1 = create_channel(s1, url)

        s2 = requests.Session()
        username2, password2, token2 = register(s2, url)
        s2.headers = {'Authorization': 'Bearer {}'.format(token2)}

        channel_name2 = create_channel(s2, url)

        channels2 = get_channel_list(s2, url)
        if find_channel_in_list(channels2, channel_name1) is None:
            raise CheckException(STATUS_MUMBLE, 'Channel list did not return required channel (different user)',
                                 str(channels2))

        channel2 = find_channel_in_list(channels2, channel_name2)
        if channel2 is None:
            raise CheckException(STATUS_MUMBLE, 'Channel list did not return required channel (same user)',
                                 str(channels2))

        random_suffix = random_string(10)
        set_welcome_message(s2, url, channel2['id'], "Welcome, %username " + random_suffix)
        invite_user(s2, url, channel2['id'], username1)
        messages2 = get_messages(s2, url, channel2['id'])

        expected_message2 = f"Welcome, {username1} {random_suffix}"
        if not any(message['text'] == expected_message2 for message in messages2):
            raise CheckException(STATUS_MUMBLE, 'Message with correct welcome message was not found',
                                 f"Expected {expected_message2}, got {str(messages2)}")

        s3 = requests.Session()
        token3 = do_login(s3, url, username1, password1)
        s3.headers = {'Authorization': 'Bearer {}'.format(token3)}

        profile3 = get_my_profile(s3, url)
        channels3 = profile3['channels']
        channel3 = find_channel_in_list(channels3, channel_name2)
        if channel3 is None:
            raise CheckException(STATUS_MUMBLE, '/user/me channel list did not return channel after being invited',
                                 str(channels3))

        expected_message3 = random_string(randint(10, 120))
        send_message(s3, url, channel3['id'], expected_message3)
        messages3 = get_messages(s3, url, channel3['id'])

        if not any(message['text'] == expected_message3 for message in messages3):
            raise CheckException(STATUS_MUMBLE, 'Message was not found after being sent',
                                 f"Expected {expected_message3}, got {str(messages3)}")

        print(STATUS_UP)
        print('Everything is fine')
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error

        exc_type, exc_obj, exc_tb = sys.exc_info()

        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during check, contact admins')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def push_flag(ip, flag):
    try:
        url = 'http://' + ip + '/'

        s = requests.Session()
        username, password, token = register(s, url)
        s.headers = {'Authorization': 'Bearer {}'.format(token)}
        channel_name = create_channel(s, url)
        channels = get_channel_list(s, url)
        channel = find_channel_in_list(channels, channel_name)

        send_message(s, url, channel['id'], flag)

        print(STATUS_UP)
        print('Everything is fine')

        return username, password, channel['id']
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error

        exc_type, exc_obj, exc_tb = sys.exc_info()

        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during check, contact admins')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def find_channel_in_list(channels: list, name: str) -> dict:
    return next((x for x in channels if x['name'] == name), None)
