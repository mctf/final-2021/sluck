### Список уязвимостей
- Подписывание JWT дефолтным ключом из конфига
- Авторизация с любым паролем из-за сравнения результата Unit функции (decodePassword)
- Чтение паролей приглашенных юзеров из-за форматирования welcome сообщения
- Забытая проверка на наличие пользователя в канале при обработке приглашения
- XSS в списке каналов