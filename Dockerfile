FROM adoptopenjdk/openjdk14:alpine
WORKDIR /tmp/compile/
COPY gradle ./gradle
COPY build.gradle.kts ./
COPY settings.gradle.kts ./
COPY gradlew ./
COPY src ./src
RUN ./gradlew --no-daemon build

FROM adoptopenjdk/openjdk14:debianslim-jre
WORKDIR /app
RUN groupadd --gid 1001 sluck && useradd --uid 1001 --gid sluck --shell /bin/bash --create-home sluck
COPY --from=0 /tmp/compile/build/libs/sluck-0.0.1-SNAPSHOT.jar app.jar
USER sluck

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
CMD ["--spring.datasource.url=jdbc:postgresql://localhost:5432/sluck"]