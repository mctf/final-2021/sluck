// noinspection JSUnresolvedVariable

'use strict';

const defaultOptions = {
    baseURL: "/api/",
    headers: {
        'Content-Type': 'application/json',
    },
};
const http = axios.create(defaultOptions);

let currentUserId = null;
let currentUsername = null;
let currentChannel = null;
let currentUserChannels = [];

let channels = [];

async function login(e) {
    e.preventDefault();

    const login = document.getElementById('login-form-login-input').value;
    const password = document.getElementById('login-form-password-input').value;

    try {
        const result = await http.post('/user/login', {
            username: login,
            encodedPassword: btoa(password)
        });

        const token = result.data.token;
        await processAuth(token);
    } catch (error) {
        printError(error);
    }
}

async function register(e) {
    e.preventDefault()

    const login = document.getElementById('register-form-login-input').value;
    const password = document.getElementById('register-form-password-input').value;

    try {
        const result = await http.post('/user/register', {
            username: login,
            encodedPassword: btoa(password)
        });

        const token = result.data.token;
        await processAuth(token);
    } catch (error) {
        printError(error);
    }
}

async function processAuth(token) {
    console.log("Got token " + token);

    http.interceptors.request.use(function (config) {
        config.headers.Authorization = `Bearer ${token}`;
        return config;
    });

    await loadUserData();
    await loadUserList();
    await loadChannelList();
    hideModal();
    startTimer();
}

function hideModal() {
    document.getElementById('auth-modal').classList.remove('active');
}

async function loadUserData() {
    try {
        const result = await http.get('/user/me');
        const data = result.data;

        console.log('User data:', data);

        currentUserId = data.id;
        currentUsername = data.username;
        currentUserChannels = data.channels;

        document.getElementById('username-label').innerText = currentUsername;
        renderUserChannels();
    } catch (error) {
        printError(error);
    }
}

function renderUserChannels() {
    const container = document.getElementById('channels-container');

    container.innerHTML = currentUserChannels.map(channel =>
        `<div class="${getChannelLabelStyles(channel.id)}">
            <a href="#" id="open-channel-${channel.id}-button" onclick="openChannel('${channel.id}')" class="no-underline text-white">
                # ${channel.name}
            </a>
        </div>`
    ).join('');
}

function getChannelLabelStyles(id) {
    if (id === currentChannel) {
        return 'bg-teal-dark py-1 px-4';
    } else {
        return 'py-1 px-4 opacity-75';
    }
}

async function loadUserList() {
    try {
        const result = await http.get('/user/list');
        const data = result.data;

        console.log('User list: ', data);

        renderUsers(data)
    } catch (error) {
        printError(error);
    }
}

function renderUsers(users) {
    const container = document.getElementById('users-container');

    let template = `
                <div class="flex items-center mb-3 px-4">
                    <span class="bg-green rounded-full block w-2 h-2 mr-2"></span>
                    <a href="#" onclick="alert('Direct messages are in development')" class="no-underline">
                        <span class="text-white opacity-75">${currentUsername} <span class="text-grey text-sm">(you)</span></span>
                    </a>
                </div>
    `;

    template += users
        .filter(user => user.username !== currentUsername)
        .map(user =>
            `
                <div class="flex items-center mb-3 px-4">
                    <span class="bg-green rounded-full block w-2 h-2 mr-2"></span>
                    <a href="#" onclick="alert('Direct messages are in development')" class="no-underline">
                        <span class="text-white opacity-75">${user.username}</span>
                    </a>
                </div>
        `
        )
        .join('');

    container.innerHTML = template;
}

async function loadChannelList() {
    try {
        const result = await http.get('/channel');
        channels = result.data;

        console.log('Channel list: ', channels);
    } catch (error) {
        printError(error);
    }
}

function toggleCreateChannelBox() {
    document.getElementById('create-channel-container').classList.toggle('container-hidden');
}

async function createChannel() {
    const input = document.getElementById('new-channel-name-input');
    const name = input.value;

    if (name === '') {
        return
    }

    input.value = '';

    try {
        await http.post('/channel/', {
            name: name
        });
    } catch (error) {
        printError(error);
    }

    await loadChannelList();
    await loadUserData();
    toggleCreateChannelBox();
}

async function openChannel(channelId) {
    currentChannel = channelId;

    renderChannelInfo();
    renderUserChannels();
    await updateMessages();
}

function renderChannelInfo() {
    const channel = channels.find(channel => channel.id === currentChannel);
    const admin = channel.members.find(member => member.isAdmin === true);
    const firstMembers = channel.members.slice(0, 4).map(m => m.username).join(', ');
    const firstMembersSuffix = channel.members.length > 4 ? ', ...' : '';

    const nameLabel = document.getElementById('channel-name-label');
    const descriptionLabel = document.getElementById('channel-description-label');
    const newMessageInput = document.getElementById('new-message-input');

    nameLabel.innerText = '# ' + channel.name;
    descriptionLabel.innerText = `Channel created by ${admin.username} at ${isoToTime(channel.createdAt)}. Members: ${firstMembers}${firstMembersSuffix}`;
    newMessageInput.placeholder = `Message #${channel.name}. Hint: try /setWelcomeMessage <message> or /invite <username>`;
}

async function updateMessages() {
    try {
        const result = await http.get(`/channel/${currentChannel}/`);
        const data = result.data;

        console.log('Messages: ', data);

        document.getElementById('chat-content-box').classList.remove('invisible');
        renderMessages(data);
    } catch (error) {
        printError(error);
    }
}

function renderMessages(messages) {
    const messagesBox = document.getElementById('chat-messages-box');

    messagesBox.innerHTML = messages
        .map(message => `
            <!-- A message -->
            <div class="flex items-start mb-4 text-sm">
                <img src="profile.png" class="w-10 h-10 rounded mr-3"/>
                <div class="flex-1 overflow-hidden">
                    <div>
                        <span class="font-bold">${message.author.username}</span>
                        <span class="text-grey text-xs">${isoToTime(message.time)}</span>
                    </div>
                    <p class="text-black leading-normal">${message.text}</p>
                </div>
            </div>
        `)
        .join('')
}

async function checkSendMessage(event) {
    if (event.keyCode === 13) {
        event.preventDefault();

        const input = document.getElementById('new-message-input');
        const message = input.value;
        if (message !== '') {
            input.value = '';
            await processSendMessage(message);
        }
    }
}

async function processSendMessage(message) {
    if (message.startsWith('/setWelcomeMessage ')) {
        await setWelcomeMessage(message.substring('/setWelcomeMessage '.length))
    } else if (message.startsWith('/invite ')) {
        await invite(message.substring('/invite '.length));
    } else {
        await sendMessage(message);
    }
}

async function invite(username) {
    try {
        await http.post(`/channel/invite`, {
            channelId: currentChannel,
            username: username
        });

        await loadChannelList();
        renderChannelInfo();
        await updateMessages();
    } catch (error) {
        printError(error);
    }
}

async function setWelcomeMessage(message) {
    try {
        await http.post(`/channel/welcomeMessage`, {
            channelId: currentChannel,
            welcomeMessage: message
        });

        await updateMessages();
    } catch (error) {
        printError(error);
    }
}

async function sendMessage(message) {
    try {
        await http.post(`/channel/${currentChannel}/`, {
            text: message
        });

        await updateMessages();
    } catch (error) {
        printError(error);
    }
}

function startTimer() {
    setInterval(updateState, 10000);
}

async function updateState() {
    await loadUserData();
    await loadUserList();
    await loadChannelList();
    
    if (currentChannel != null) {
        await updateMessages();
    }
}

function isoToTime(iso) {
    return new Date(Date.parse(iso)).toLocaleTimeString("en-GB");
}

function printError(error) {
    const message = error.response?.data?.message ?? error;

    alert(message);
    console.log(message);
}
