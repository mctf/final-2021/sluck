package ru.mctf.sluck

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class SluckApplication

fun main(args: Array<String>) {
    runApplication<SluckApplication>(*args)
}

inline fun <reified T> T.logger(): Logger =
    LoggerFactory.getLogger(T::class.java)