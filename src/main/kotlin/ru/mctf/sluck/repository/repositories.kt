package ru.mctf.sluck.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.mctf.sluck.model.dao.Channel
import ru.mctf.sluck.model.dao.User
import java.time.Instant
import java.util.*

@Repository
interface ChannelRepository : JpaRepository<Channel, UUID> {

    fun existsByName(name: String): Boolean

    fun deleteAllByAdminIn(users: List<User>)

}

@Repository
interface UserRepository : JpaRepository<User, UUID> {

    fun existsByUsername(username: String): Boolean

    fun findByUsername(username: String): User?

    fun findAllByRegisterDateBefore(date: Instant): List<User>

}