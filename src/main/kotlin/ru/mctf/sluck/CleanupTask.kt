package ru.mctf.sluck

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import ru.mctf.sluck.repository.ChannelRepository
import ru.mctf.sluck.repository.UserRepository
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit


@Component
class CleanupTask(
    val channelRepository: ChannelRepository,
    val userRepository: UserRepository
) {

    private val log = logger()

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
    @Transactional
    fun cleanup() {
        log.info("Running cleanup")

        val threshold = Instant.now().minus(10, ChronoUnit.MINUTES)
        val oldUsers = userRepository.findAllByRegisterDateBefore(threshold)

        log.info("Deleting ${oldUsers.size} users")

        oldUsers.forEach { user ->
            user.channels.forEach {
                it.members.remove(user)
            }

            channelRepository.saveAll(user.channels)
        }

        channelRepository.deleteAllByAdminIn(oldUsers)
        userRepository.deleteAll(oldUsers)
    }

}