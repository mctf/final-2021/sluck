package ru.mctf.sluck.model.dto

data class ChannelCreateDto(
    val name: String
)
