package ru.mctf.sluck.model.dto

import java.util.*

data class ChannelSetWelcomeMessageDto(
    val channelId: UUID,
    val welcomeMessage: String
)