package ru.mctf.sluck.model.dto

import ru.mctf.sluck.model.dao.Channel
import java.time.Instant
import java.util.*

data class ChannelDto(
    val id: UUID,
    val name: String,
    val welcomeMessage: String,
    val createdAt: Instant,
    val members: List<ChannelMemberDto>
)

data class UserChannelDto(
    val id: UUID,
    val name: String
)

fun Channel.toDto() = ChannelDto(
    id = checkNotNull(id),
    name = name,
    welcomeMessage = welcomeMessage,
    createdAt = creationDate,
    members = members.map {
        it.toChannelMemberDto(
            isAdmin = (it.id == admin.id)
        )
    }
)

fun Channel.toUserChannelDto() = UserChannelDto(
    id = checkNotNull(id),
    name = name,
)