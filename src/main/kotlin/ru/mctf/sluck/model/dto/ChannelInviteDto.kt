package ru.mctf.sluck.model.dto

import java.util.*

data class ChannelInviteDto(
    val username: String,
    val channelId: UUID
)