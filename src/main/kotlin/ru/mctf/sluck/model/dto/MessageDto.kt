package ru.mctf.sluck.model.dto

import ru.mctf.sluck.model.dao.Message
import java.time.Instant

data class MessageDto(
    val author: UserDto,
    val text: String,
    val time: Instant
)

fun Message.toDto() = MessageDto(
    author = author.toDto(),
    text = content,
    time = time
)