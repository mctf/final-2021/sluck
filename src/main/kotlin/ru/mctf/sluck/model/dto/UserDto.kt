package ru.mctf.sluck.model.dto

import ru.mctf.sluck.model.dao.User
import java.util.*

data class UserDto(
    val id: UUID,
    val username: String
)

data class ChannelMemberDto(
    val id: UUID,
    val username: String,
    val isAdmin: Boolean
)

data class MyUserDescription(
    val id: UUID,
    val username: String,
    val channels: List<UserChannelDto>
)

fun User.toDto() = UserDto(
    id = checkNotNull(id),
    username = username
)

fun User.toChannelMemberDto(isAdmin: Boolean) = ChannelMemberDto(
    id = checkNotNull(id),
    username = username,
    isAdmin = isAdmin
)

fun User.toMyUserDto() = MyUserDescription(
    id = checkNotNull(id),
    username = username,
    channels = channels.map { it.toUserChannelDto() }
)