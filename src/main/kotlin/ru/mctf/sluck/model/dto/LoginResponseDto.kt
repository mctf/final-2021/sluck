package ru.mctf.sluck.model.dto

data class LoginResponseDto(
    val token: String
)
