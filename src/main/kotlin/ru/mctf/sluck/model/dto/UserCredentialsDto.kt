package ru.mctf.sluck.model.dto

data class UserCredentialsDto(
    val username: String,
    val encodedPassword: String
)