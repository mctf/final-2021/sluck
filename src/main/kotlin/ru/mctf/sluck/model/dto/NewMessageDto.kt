package ru.mctf.sluck.model.dto

data class NewMessageDto(
    val text: String
)
