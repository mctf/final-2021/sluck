package ru.mctf.sluck.model.dao

import java.time.Instant
import java.util.*
import javax.persistence.*

@Table(name = "users")
@Entity
class User(
    @Id
    @GeneratedValue
    val id: UUID? = null,

    @Column(nullable = false)
    val username: String,

    @Column(nullable = false)
    val encodedPassword: String,

    @Column(nullable = false)
    val registerDate: Instant = Instant.now()
) {

    @OrderBy("name ASC")
    @ManyToMany(mappedBy = "members")
    var channels: MutableList<Channel> = mutableListOf()
}