package ru.mctf.sluck.model.dao

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
class Message(
    @Id
    @GeneratedValue
    val id: UUID? = null,

    @ManyToOne(optional = false)
    @JoinColumn(name = "author_id", nullable = false)
    val author: User,

    @Column(nullable = false, length = 511)
    val content: String,

    @Column(nullable = false)
    val time: Instant = Instant.now()
)