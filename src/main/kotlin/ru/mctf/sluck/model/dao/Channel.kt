package ru.mctf.sluck.model.dao

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
class Channel(
    @Id
    @GeneratedValue
    val id: UUID? = null,

    @Column(nullable = false)
    val name: String,

    @ManyToOne(optional = false)
    @JoinColumn(name = "admin_id", nullable = false)
    val admin: User,

    @Column(nullable = false)
    val creationDate: Instant = Instant.now()
) {

    @Column(nullable = false)
    var welcomeMessage: String = "Добро пожаловать, %username"

    @OrderBy("time ASC")
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "channel_id")
    var messages: MutableList<Message> = mutableListOf()

    @OrderBy("username ASC")
    @ManyToMany
    @JoinTable(
        name = "channel_members",
        joinColumns = [JoinColumn(name = "channel_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    var members: MutableList<User> = mutableListOf()

}