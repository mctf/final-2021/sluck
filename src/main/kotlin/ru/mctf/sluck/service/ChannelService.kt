package ru.mctf.sluck.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import ru.mctf.sluck.logger
import ru.mctf.sluck.model.dao.Channel
import ru.mctf.sluck.model.dao.Message
import ru.mctf.sluck.model.dao.User
import ru.mctf.sluck.model.dto.*
import ru.mctf.sluck.repository.ChannelRepository
import java.util.*
import kotlin.reflect.full.declaredMemberProperties

@Service
class ChannelService(
    val repository: ChannelRepository,
    val userService: UserService
) {

    private val log = logger()

    fun createChannel(input: ChannelCreateDto) {
        if (repository.existsByName(input.name)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Channel with this name already exists")
        }

        val user = userService.getCurrentUserOrFail()

        val channel = Channel(
            name = input.name,
            admin = user
        )
        channel.members.add(user)

        repository.save(channel)
    }

    fun createMessage(channelId: UUID, input: NewMessageDto) {
        val channel = getChannel(channelId)

        val user = userService.getCurrentUserOrFail()
        val text = input.text.let {
            if (it.length > 511) it.substring(0, 508) + "..."
            else it
        }

        if (!channel.members.contains(user)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN)
        }

        val message = Message(author = user, content = text)
        channel.messages.add(message)

        repository.save(channel)
    }

    fun addUserToChannel(input: ChannelInviteDto) {
        val inviter = userService.getCurrentUserOrFail()
        val channel = getChannel(input.channelId)
        val user = userService.getByUsername(input.username)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")

        if (channel.members.contains(user)) {
            return
        }

        val welcomeMessage = channel.welcomeMessage
        channel.messages.add(
            Message(
                author = inviter,
                content = formatWelcomeMessage(welcomeMessage, user)
            )
        )

        channel.members.add(user)
        repository.save(channel)
    }

    private fun formatWelcomeMessage(message: String, user: User): String {
        return message.replace(Regex("%([A-Za-z]+)")) { variable ->
            User::class.declaredMemberProperties.find { it.name == variable.groupValues[1] }?.get(user).toString()
        }
    }

    fun setWelcomeMessage(input: ChannelSetWelcomeMessageDto) {
        val channel = getChannel(input.channelId)
        val user = userService.getCurrentUserOrFail()

        if (channel.admin != user) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN)
        }

        channel.welcomeMessage = input.welcomeMessage
        channel.messages.add(
            Message(
                author = user,
                content = "Updated welcome message"
            )
        )

        repository.save(channel)
    }

    fun listChannels(): List<ChannelDto> {
        return repository.findAll().map { it.toDto() }
    }

    fun listMessages(channelId: UUID): List<MessageDto> {
        val user = userService.getCurrentUserOrFail()
        val channel = getChannel(channelId)

        if (!channel.members.contains(user)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN)
        }

        return channel.messages.map { it.toDto() }
    }

    fun getChannel(channelId: UUID): Channel {
        return repository.findByIdOrNull(channelId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Channel id not found")
    }

}