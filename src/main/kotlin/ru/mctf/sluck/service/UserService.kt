package ru.mctf.sluck.service

import org.springframework.data.domain.Sort
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import ru.mctf.sluck.configuration.jwt.CustomUserDetails
import ru.mctf.sluck.logger
import ru.mctf.sluck.model.dao.User
import ru.mctf.sluck.model.dto.*
import ru.mctf.sluck.repository.UserRepository
import java.util.*

@Service
class UserService(
    val repository: UserRepository
) {

    private val usernamePattern = Regex("[A-Za-z0-9]{4,20}")

    private val log = logger()

    fun createUser(input: UserCredentialsDto): UserDto {
        validateCredentialsFormat(input)

        if (repository.existsByUsername(input.username)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists")
        }

        try {
            decodePassword(input.encodedPassword)
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect password format")
        }

        val user = repository.save(
            User(
                username = input.username,
                encodedPassword = input.encodedPassword
            )
        )

        return user.toDto()
    }

    private fun validateCredentialsFormat(input: UserCredentialsDto) {
        if (!input.username.matches(usernamePattern)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect username")
        }

        if (input.encodedPassword.isBlank()) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect password")
        }
    }

    fun authenticateUser(input: UserCredentialsDto): UserDto {
        val user = getByUsername(input.username)
            ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "User does not exist")

        if (decodePassword(user.encodedPassword) != decodePassword(input.encodedPassword)) {
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Incorrect password")
        }

        return user.toDto()
    }

    fun listUsers(): List<UserDto> {
        return repository.findAll(Sort.by("username")).map { it.toDto() }
    }

    fun getUser(id: UUID): User? {
        return repository.findByIdOrNull(id)
    }

    fun getByUsername(username: String) : User? {
        return repository.findByUsername(username)
    }

    fun getUserDescription(): MyUserDescription {
        return getCurrentUserOrFail().toMyUserDto()
    }

    fun getCurrentUser(): User? {
        val context = SecurityContextHolder.getContext() ?: return null
        val auth = context.authentication ?: return null

        if (!auth.isAuthenticated || auth is AnonymousAuthenticationToken) {
            return null
        }

        val principal = auth.principal as CustomUserDetails

        return getUser(principal.id)
    }

    fun getCurrentUserOrFail() : User = getCurrentUser() ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED)

    private fun decodePassword(encodedPassword: String) {
        String(Base64.getDecoder().decode(encodedPassword))
    }

}