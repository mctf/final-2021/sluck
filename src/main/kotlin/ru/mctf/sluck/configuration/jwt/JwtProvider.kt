package ru.mctf.sluck.configuration.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.mctf.sluck.logger

@Component
class JwtProvider(
    @Value("\${jwt.secret}")
    private val jwtSecret: String
) {

    val log = logger()

    fun generateToken(username: String): String {
        return Jwts.builder()
            .setSubject(username)
            .signWith(SignatureAlgorithm.HS512, jwtSecret.encodeToByteArray())
            .compact()
    }

    fun validateToken(token: String): Boolean {
        return try {
            Jwts.parser()
                .setSigningKey(jwtSecret.encodeToByteArray())
                .parseClaimsJws(token)
            true
        } catch (e: Exception) {
            log.warn("JWT validation error", e)
            false
        }
    }

    fun getUsernameFromToken(token: String): String {
        val claims = Jwts.parser()
            .setSigningKey(jwtSecret.encodeToByteArray())
            .parseClaimsJws(token)
            .body

        return claims.subject
    }

}