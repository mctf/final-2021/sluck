package ru.mctf.sluck.configuration.jwt

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import ru.mctf.sluck.model.dao.User
import java.util.*

class CustomUserDetails(
    val id: UUID,
    private val username: String,
    private val encodedPassword: String
) : UserDetails {

    override fun getUsername() = username

    override fun getPassword() = encodedPassword

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return listOf(SimpleGrantedAuthority("ROLE_USER"))
    }

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true
}

fun User.toUserDetails() = CustomUserDetails(
    id = checkNotNull(id),
    username = username,
    encodedPassword = encodedPassword
)