package ru.mctf.sluck.controller

import org.springframework.web.bind.annotation.*
import ru.mctf.sluck.model.dto.MessageDto
import ru.mctf.sluck.model.dto.NewMessageDto
import ru.mctf.sluck.service.ChannelService
import java.util.*

@RestController
@RequestMapping("/api/channel/{channelId}")
class MessageController(
    val channelService: ChannelService
) {

    @PostMapping
    fun create(@PathVariable channelId: UUID, @RequestBody input: NewMessageDto) {
        channelService.createMessage(channelId, input)
    }

    @GetMapping
    fun list(@PathVariable channelId: UUID): List<MessageDto> {
        return channelService.listMessages(channelId)
    }

}