package ru.mctf.sluck.controller

import org.springframework.web.bind.annotation.*
import ru.mctf.sluck.model.dto.ChannelCreateDto
import ru.mctf.sluck.model.dto.ChannelDto
import ru.mctf.sluck.model.dto.ChannelInviteDto
import ru.mctf.sluck.model.dto.ChannelSetWelcomeMessageDto
import ru.mctf.sluck.service.ChannelService

@RestController
@RequestMapping("/api/channel")
class ChannelController(
    val service: ChannelService
) {

    @PostMapping
    fun create(@RequestBody input: ChannelCreateDto) {
        service.createChannel(input)
    }

    @GetMapping
    fun list(): List<ChannelDto> {
        return service.listChannels()
    }

    @PostMapping("/invite")
    fun invite(@RequestBody input: ChannelInviteDto) {
        service.addUserToChannel(input)
    }

    @PostMapping("/welcomeMessage")
    fun setWelcomeMessage(@RequestBody input: ChannelSetWelcomeMessageDto) {
        service.setWelcomeMessage(input)
    }

}