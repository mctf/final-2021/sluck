package ru.mctf.sluck.controller

import org.springframework.web.bind.annotation.*
import ru.mctf.sluck.configuration.jwt.JwtProvider
import ru.mctf.sluck.model.dto.LoginResponseDto
import ru.mctf.sluck.model.dto.MyUserDescription
import ru.mctf.sluck.model.dto.UserCredentialsDto
import ru.mctf.sluck.model.dto.UserDto
import ru.mctf.sluck.service.UserService

@RestController
@RequestMapping("/api/user")
class UserController(
    val service: UserService,
    val jwtProvider: JwtProvider
) {

    @PostMapping("/register")
    fun register(@RequestBody input: UserCredentialsDto): LoginResponseDto {
        val user = service.createUser(input)
        val token = jwtProvider.generateToken(user.username)
        return LoginResponseDto(token)
    }

    @PostMapping("/login")
    fun login(@RequestBody input: UserCredentialsDto): LoginResponseDto {
        val user = service.authenticateUser(input)
        val token = jwtProvider.generateToken(user.username)
        return LoginResponseDto(token)
    }

    @GetMapping("/me")
    fun me(): MyUserDescription {
        return service.getUserDescription()
    }

    @GetMapping("/list")
    fun listUsers(): List<UserDto> {
        return service.listUsers()
    }

}